#include <iostream>

using namespace std;

class Player
{
private:
	string name{};
	int score{0};

public:

	string getName();
	int getScore();

	void setName(string t);
	void setScore(int x);
	void outputStats();
};



string Player::getName()
{
	return name;
}

int Player::getScore()
{
	return score;
}

void Player::setName(string t)
{
	name = t;
}

void Player::setScore(int x)
{
	score = x;
}

void Player::outputStats()
{
	cout << name << ": " << score << endl;
}



int random(int min, int max) {
	return min + rand() / (RAND_MAX / (max - min + 1) + 1);
}



int main()
{
	Player blank;
	blank.setName("NULL");
	blank.setScore(99);

	int c;
	cout << "Player count: ";
	cin >> c;

	Player* p = new Player[c];

	string* names = new string[c];
	int* points = new int[c];

	for (int i = 0; i < c; i++)
	{
		cout << "Player " << i + 1 << ": ";
		cin >> names[i];

		cout << "Player " << i + 1 << " Score: ";
		cin >> points[i];

		p[i].setName(names[i]);
		p[i].setScore(points[i]);
	}

	cout << "\nLoading Leaderboard...\n\n";

	for (int j = 0; j < c; j++)
	{
		for (int i = 0; i < c - 1; i++)
		{
			if (p[i].getScore() < p[i + 1].getScore())
			{
				blank = p[i];
				p[i] = p[i + 1];
				p[i + 1] = blank;
			}
		}
	}

	for (int i = 0; i < c; i++)
	{
		cout << p[i].getName() << ": " << p[i].getScore() << "\n\n";
	}

	delete[] p;
	return 0;
}